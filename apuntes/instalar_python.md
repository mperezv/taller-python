### ¿Cómo instalo python?

Antes que nada, comprueba que no esté instalado ya:
* `python --version`

Si no está instalado, tendrás que instalarlo:
* Debian based: `sudo apt-get install python`
* Arch based: `pacman -S`
`yu pacman -S python`
* Centos / Redhat / Fedora: `yum install python3`
* OpenSUSE: `zypper in python3`
* Windows: descargar el executable installer desde la [web de python](https://www.python.org/downloads/windows/), probablemente el x86-64. Es importante marcar la casilla Add Python to PATH o Add Python to your environment variables.

### ¿Y ahora qué?
¡Ahora podemos ejecutar scripts en python!

Para ejecutar un script escrito en python tenemos que escribir en la línea de comandos:
    
`python3 <nombre_de_mi_archivo>`

Tambien podemos ejecutar una consola de python desde la shell para probar pequeñas órdenes, o usarla de calculadora que queda muy jaker.
    
`python3`

### ¿Y dónde escribo mis scripts?
Podemos utilizar una IDE (entorno de desarrollo) o el bloc de notas, de momento si utilizamos el bloc de notas no pasa nada porque haremos cosas muy pequeñitas pero si hacemos proyectos más grandes es muy recomendable utilizar una IDE para facilitarnos la vida. 

Hay muchas IDEs diferentes, esto va a gustos u obligaciones.

* Vim: la libre, potente y difícil de aprender.
* PyCharm: muy usada, no es libre pero tiene una versión gratuita (Community).
* Visual Studio Code: es de Microsoft pero la verdad es que funciona bastante bien.
* NetBeans: GPL y soporta un montón de lenguajes.



