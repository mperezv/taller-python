# Recursos útiles
* [Python Cheatsheet](https://www.pythoncheatsheet.org/)
* [Awesome Python](https://github.com/vinta/awesome-python)
* [Code Academy](https://www.codecademy.com/catalog/language/python)
* [Exercism](https://exercism.io/)
* [Python Challenge](http://www.pythonchallenge.com/)
