# Condicionales y operadores

(W3Schools, _Operadores en Python_ )[https://www.w3schools.com/python/python_operators.asp]

"Una sentencia condicional es una instrucción o grupo de instrucciones que se pueden ejecutar o no en función del valor de una condición."[Wikipedia, _Sentencia condicional_](https://es.wikipedia.org/wiki/Sentencia_condicional) 

Las condiciones que se usan en python son SI(if), Y SINO, SI(elif) y SINO(else).

Por ejemplo:

SI valor1 es mayor a valor2:
    
    print("valor1 es mayor")

Y SINO SI valor1 es igual a valor2:
    
    print("valor1 y valor2 son iguales")

SINO:
    
    print("valor1 es menor")


Esto, en python sería algo así:
```python
    valor1 = 0
    valor2 = 1

    if valor1 > valor2:
        print("valor1 es mayor")
    elif valor1 == valor2:
        print("valor1 y valor2 son iguales")
    else
        print("valor1 es menor")
```

Las condiciones son valores booleanos. Es decir, true o false.

Por eso, podemos conformarlas tanto por operadores lógicos y de comparación (==, !=, <, >, <=, >=, and, or, not, is, is not), como por variables boolean.

```python
valor_booleano = true

if valor_booleano: 
    print("Dice la verdad")
else:
    print("Miente")
```