# Datos y variables

## Tipos de datos
"Un tipo de dato informático es un atributo de los datos que indica al ordenador (y/o al programador/programadora) sobre la clase de datos que se va a manejar. Esto incluye imponer restricciones en los datos, como qué valores pueden tomar y qué operaciones se pueden realizar." [Wikipedia, _Tipo de dato_](https://es.wikipedia.org/wiki/Tipo_de_dato)  

Existen los tipos de datos primitivos y los tipos de datos compuestos.

### Tipos de datos primitivos
##### Caracteres
* character

##### Números enteros
* byte
* short
* int
* long

##### Números reales (con decimales)
* float
* double

##### Booleanos (true/false)
* boolean

##### Null
* Depende del lenguaje de programación; en python es None (en Java es `null`, en Ruby es `nil`, en PHP es `NULL`... y así)

### Tipos compuestos
También se les llaman estructura de datos.
* Vectores o _arrays_: pueden ser estáticos o dinámicos
* Registros o Tuplas
* Conjuntos o _sets_

> Para saber el tipo de dato de una variable en python podemos utilizar la función type():
```python
x = 5
print(type(x))
```

## Variables
Las variables son contenedores para guardar datos. 

Prueba a incializar python en tu shell y a declarar una variable:

`python3`

```python
x = 3
print(x)
```
En python no es necesario declarar el tipo de dato al declarar una variable y ésta puede cambiar de tipo en cualquier momento sin que se queje.

```python
x=3 #tipo integer
print(x)
x="Hola"#tipo string
print(x)
```

