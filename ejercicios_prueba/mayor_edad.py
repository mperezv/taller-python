val = input("¿Cuántos años tienes?\n") #Esto recoge strings

if int(val)>=18: #Esto se llama casting
    print("Eres mayor de edad")
#También hay elif
else:
    print("Eres menor de edad")

#El core de python no incorpora switch-case, pero se puede implementar si a alguien le interesa